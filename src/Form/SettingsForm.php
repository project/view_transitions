<?php declare(strict_types = 1);

namespace Drupal\view_transitions\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure View Transitions settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'view_transitions_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['view_transitions.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['general_options'] = array(
      '#type' => 'fieldset',
      '#title' => $this
        ->t('General Options'),
    );
    $form['general_options']['page_transitions'] = [
      '#type' => 'radios',
      '#title' => $this->t('Transition mode'),
      '#default_value' => $this->config('view_transitions.settings')->get('page_transitions'),
      '#options' => [
        'off' => $this->t('Off'),
        'spa' => $this->t('Single-Page Application Mode'),
        'mpa' => $this->t('Multi-Page Application Mode (Experimental)'),
      ],
      'off' => [
        '#description' => $this->t('Disable full page view transitions.'),
      ],
      'spa' => [
        '#description' => $this->t('Enable full page view transitions using client-side routing.'),
      ],
      'mpa' => [
        '#description' => $this->t('Enable full page view transitions using server-side routing. Currently behind a flag in Chromium browsers.'),
      ],
    ];
    $form['general_options']['transition_in_admin'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable view transitions on administration pages'),
      '#default_value' => $this->config('view_transitions.settings')->get('transition_in_admin'),
      '#states' => [
        'invisible' => [
          ':input[name="page_transitions"]' => ['value' => 'off'],
        ],
      ],
    );
    $form['page_transition_options'] = array(
      '#type' => 'fieldset',
      '#title' => $this
        ->t('Page Transition Options'),
      '#states' => [
        // Show only if a page transition mode is enabled.
        'invisible' => [
          ':input[name="page_transitions"]' => ['value' => 'off'],
        ],
      ],
    );
    $form['page_transition_options']['animation_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Animation Type'),
      '#default_value' => $this->config('view_transitions.settings')->get('animation_type'),
      '#description' => $this->t('Non-default animations apply only to browsers that support view transitions.'),
      '#options' => [
        'default' => $this->t('Cross-fade (default)'),
        'slide' => $this->t('Slide (left to right)'),
        'circle' => $this->t('Circle'),
        'shutter' => $this->t('Shutter'),
        'wipe' => $this->t('Wipe (diagonal)'),
      ]
    ];
    $form['element_transition_options'] = array(
      '#type' => 'fieldset',
      '#title' => $this
        ->t('Element Transition Options'),
      '#states' => [
        // Show only if a page transition mode is enabled.
        'invisible' => [
          ':input[name="page_transitions"]' => ['value' => 'off'],
        ],
      ],
    );
    $form['element_transition_options']['transition_nodes'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable transitions for node elements'),
      '#default_value' => $this->config('view_transitions.settings')->get('transition_nodes'),
      '#description' => $this->t('Adds a unique view transition name to node templates. Works well with things like node teaser lists.'),
      '#states' => [
        'invisible' => [
          ':input[name="page_transitions"]' => ['value' => 'off'],
        ],
      ],
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // @todo Validate the form here.
    // Example:
    // @code
    //   if ($form_state->getValue('example') === 'wrong') {
    //     $form_state->setErrorByName(
    //       'message',
    //       $this->t('The value is not correct.'),
    //     );
    //   }
    // @endcode
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('view_transitions.settings')
      ->set('page_transitions', $form_state->getValue('page_transitions'))
      ->set('animation_type', $form_state->getValue('animation_type'))
      ->set('transition_in_admin', $form_state->getValue('transition_in_admin'))
      ->set('transition_nodes', $form_state->getValue('transition_nodes'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
