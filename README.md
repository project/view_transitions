## INTRODUCTION

Enable cross-page transitions using the view transitions API.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.
